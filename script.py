import datetime

import csv

def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end


with open('challenge/access_log.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_access_log = next(reader)
    data_access_log = [line for line in reader]

with open('challenge/account_company.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_account_company = next(reader)
    data_account_company = [line for line in reader]

with open('challenge/account_external.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_account_external = next(reader)
    data_account_external = [line for line in reader]

with open('challenge/authorization_level.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_authorization_level = next(reader)
    data_authorization_level = [line for line in reader]


with open('challenge/bank_transfer_file.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_bank_transfer_file = next(reader)
    data_bank_transfer_file = [line for line in reader]


with open('challenge/calendar.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_calendar = next(reader)
    data_calendar = [line for line in reader]

with open('challenge/department.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_department = next(reader)
    data_department = [line for line in reader]

with open('challenge/employee.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_employee = next(reader)
    data_employee = [line for line in reader]

with open('challenge/employee_department.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_employee_department = next(reader)
    data_employee_department = [line for line in reader]


with open('challenge/invoice_purchase_order.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_invoice_purchase_order = next(reader)
    data_invoice_purchase_order = [line for line in reader]


with open('challenge/invoice_transactions.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_invoice_transactions = next(reader)
    data_invoice_transactions = [line for line in reader]


with open('challenge/public_holidays_zrh.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_public_holidays_zrh = next(reader)
    data_public_holidays_zrh = [line for line in reader]


with open('challenge/system_users.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_system_users = next(reader)
    data_system_users = [line for line in reader]


with open('challenge/transactions.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_transactions = next(reader)
    data_transactions = [line for line in reader]


with open('challenge/user_groups.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_user_groups = next(reader)
    data_user_groups = [line for line in reader]


with open('challenge/vendor.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_vendor = next(reader)
    data_vendor = [line for line in reader]


with open('challenge/vendor_address.csv') as f:
    reader = csv.reader(f,delimiter='|')
    headers_vendor_address = next(reader)
    data_vendor_address = [line for line in reader]

###############DONE####################
#check dimension of the vendor_address csv file
length = len(headers_vendor_address)
#how many of them miss smth
col = 0
#set for not repeating elements
vendor = set()
#look for '' somewhere in the information
for row in data_vendor_address:
	for i in range(length):
		if row[i]=='':
			print(row)
			vendor.add(row[1])
			col += 1
#overall misses
print(col)
#num of vendors
print(len(vendor))
print(vendor)
###############DONE####################

print(headers_calendar)
print(data_calendar[0][0][:10])

calendar_dictionary = {}
offdays = []
for i,row in enumerate(data_calendar):
	if data_calendar[i][headers_calendar.index('PublicHolidays')] == 'Y' or data_calendar[i][headers_calendar.index('WeekEndDay')] == 'Y':
		offdays.append(data_calendar[i][0][:10])
	#else:
		#calendar_dictionary[data_calendar[i][headers_calendar.index('\ufeffCalendarDate')][:10]] = data_calendar[i][headers_calendar.index('Wday')]
		
#print('2007-12-25' in offdays)

'''for objs in lines2:
    datetimeobj = (datetime.strptime(objs,'%Y-%m-%d %H:%M:%S'))
    times.append(datetimeobj.time().hour)'''

#print(data_access_log[0][0])

#datetimeobj = (datetime.strptime(data_access_log[0][0],'%Y-%m-%d %H:%M:%S'))
#print(datetimeobj.time().hour)

'''tt = data_access_log[0][0][11:]
hours,minutes,seconds = map(int,tt.split(':'))
print(hours,minutes,seconds)
start_day = datetime.time(9,0,0)
end_day = datetime.time(18,0,0)
print(start_day,end_day)

print(time_in_range(start_day,end_day,datetime.time(hours, minutes, seconds)))'''


start_day = datetime.time(7,0,0)
end_day = datetime.time(20,0,0)
print(start_day,end_day)
col = 0
print(col)

print(len(offdays))

'''for i,row in enumerate(data_access_log):
	if data_access_log[i][1] == 'u8284':
		col += 1'''


susp = set()
for i,row in enumerate(data_access_log):
	tt = data_access_log[i][0][11:]
	hours,minutes,seconds = map(int,tt.split(':'))
	if data_access_log[i][0][:10] in offdays or not time_in_range(start_day,end_day,datetime.time(hours, minutes, seconds)):
		col += 1
		susp.add(data_access_log[i][0])
		#if not time_in_range(start_day,end_day,datetime.time(hours, minutes, seconds)):
			#col+=1
			#susp.add(data_access_log[i][0])
print(col)
#print(susp)
print(len(susp))

print(time_in_range(start_day,end_day,datetime.time(8, 59, 42)))





#hours_von, minutes_von = map(int, von.split(':'))
#hours_bis, minutes_bis = map(int, bis.split(':'))
 
##start = datetime.time(hours_von,minutes_von,0)
#end = datetime.time(hours_bis,minutes_bis,0)
'''for u,v in enumerate(range(20,30)):
	print(u,v)'''




